/**
 * Gallery
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
      albumID: {
          type: 'integer',
          autoIncrement: true
      },
      albumName: {
          type: 'string',
          unique: true
      },
      albumDescription: ''

  	/* e.g.
  	nickname: 'string'
  	*/
    
  }

};
