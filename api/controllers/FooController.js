/**
 * FooController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

var fs = require("fs");
var dirs = {};
module.exports = {
    
  


  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to FooController)
   */

  _config: {},
  getPhotoDirs: function (req, res){
	fs.readdir(".tmp/public/images/gallery/", function (err, files) {
        if (err)return action(err);
		files.forEach(function (file) {
		    var filePath = ".tmp/public/images/gallery/"+file;
		    var stat = fs.statSync(filePath);
		    if (stat.isDirectory()){
                fs.readdir(filePath, function(err1, files1){
                    if (err1) throw err1;

                    var photos = new Array();
                    files1.forEach(function (photo) {
                        photos.push(photo);
                    });
                    dirs[file]=photos;
                });
		    }
		});
		res.send(dirs);
	});

  }

};
