/**
 * app.js
 *
 * This file contains some conventional defaults for working with Socket.io + Sails.
 * It is designed to get you up and running fast, but is by no means anything special.
 *
 * Feel free to change none, some, or ALL of this file to fit your needs!
 */

(function (io) {

  // as soon as this file is loaded, connect automatically, 
  var mulligan = io.connect();

  mulligan.on('connect', function socketConnected() {

    // Listen for Comet messages from Sails
    mulligan.on('message', function messageReceived(message) {

      ///////////////////////////////////////////////////////////
      // Replace the following with your own custom logic
      // to run when a new message arrives from the Sails.js
      // server.
      ///////////////////////////////////////////////////////////
      log('New comet message received :: ', message);
      //////////////////////////////////////////////////////

    });

    ///////////////////////////////////////////////////////////
    var categories = [];
    var animsArray = ["slide-left","fade","slide-right","scale-up","scale-down","slide-top","slide-bottom"];
    function randomize(array){
        var randomVal = Math.floor(Math.random() * array.length);
        return array[randomVal];
    }
    mulligan.get("/foo/getPhotoDirs", function(response) {
        categories = response;
        $.each(categories, function( index, value ) {
            var categoryID = index.replace(/\s/g, "");
            $('#galleryDropdown').append(" <li> <a href='#"+categoryID+"' data-uk-smooth-scroll>"+ index + "</a></li>");
            $('#categoryInput').append(" <option> "+ index + "</option>");
            var categoryPath = "../images/gallery/"+index+"/";
            $('#galleryTopFive').append( "<div class='uk-width-1-1'>" +
                "<hr class='uk-grid-divider uk-margin-large' id='"+categoryID+"'> </hr>" +
                "<h3 class='uk-text-center uk-h2 uk-margin'> " + index + "</h3>" +
                "<hr class='uk-grid-divider uk-margin-large'> </hr>" +
                "<div class='uk-grid uk-grid-small'>" +
                    "<br class='uk-clearfix uk-margin-large' id='md-"+categoryID+"'>" +
                    "<a class='uk-button-primary uk-button-large uk-float-right uk-margin-large-right' href=''>View more "+index+"</a></div> " +
                "</div>");
            var count = 0;
            $.each(value, function (index1, value1) {
                var fileName = value1.replace(/\.[^/.]+$/, "");
                if (count < 5 ) {
                    var completeFilePath = categoryPath + value1;
                    $('#md-'+categoryID).before("" +
                        "<a class='uk-thumbnail uk-width-medium-1-5 uk-margin ' data-uk-scrollspy='{cls:\"uk-animation-"+randomize(animsArray)+"\", repeat: true}' href='#"+fileName+"'>"+
                            "<img class='favorite-thumb' src='"+completeFilePath+"' alt=''>"+
                        "</a>");
                      count++
                } else { }
            })
        });
    });
    
  });


  // Expose connected `socket` instance globally so that it's easy
  // to experiment with from the browser console while prototyping.
  window.mulligan = mulligan;


  // Simple log function to keep the example simple
  function log () {
    if (typeof console !== 'undefined') {
      console.log.apply(console, arguments);
    }
  }
  

})(

  // In case you're wrapping socket.io to prevent pollution of the global namespace,
  // you can replace `window.io` with your own `io` here:
  window.io
  
);

