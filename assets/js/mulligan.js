/**
 * @author Aaron
 */

$(document).ready(function($) {
  $('#md-slideshow').bjqs({
    'height' : 420,
    'width' : 940,
    'animduration' : 500,
    'animspeed' : 3000,
    'responsive' : true
  });

});